# GitLab Tutorial designed for the SSD group 

This GitLab project serves as a tutorial project for the SSD group.

All kinds of testing and exploring should be done here.

### Download git

- Linux: Execute in terminal `sudo apt install git`
- Windows: Available at `https://git-scm.com/download/win`
- MacOS: Execute in termianl `xcode-select --install` (installs command line tools, which also contain git) or wth Homebrew `brew install git`

## Typical working routine

The fist thing you want to do is to check if you are on the correct branch.
1. `git branch -a`

If you are on the wrong branch you can switch to the correct one, using `git checkout BRANCH_NAME`. If are at the correct branch, check the status of your repository, to verify that your branch is on the current status of the remote branch.

2. `git status`

If your branch is on an earlier status than the remote branch, you have to execute `git pull`. If your local branch is ahead of the remote branch, check if the changes are wanted (`git status` to identify which files are different.), and execute `git commit -m 'DESCRIPTION_OF_COMMIT'` and upload the branch to the remote repository (`git push`). If the changes are unwanted, remove them with `git checkout -- FILENAME` (if you want to permanently remove them) or `git stash` (discards all local changes, but saves them for later use). If your repository is now in the same state as the remote repository you can start working and editing all files like you want.
If you are done with your work, the git routine continues, by adding your work to the branch.

3. `git add FILENAME` of `git add --all`

Now your changes are added to the local repository. To save them you need to commit them.

4. `git commit -m 'DESCRIPTION_OF_COMMIT'`

After they are committed, you can upload them to the remote repository.

5. `git push origin BRANCH_NAME` or `git push`

## Cheat sheet

`git status`: Check status of repository compared to pushed branch version.

`git log`: See last commits to the repository.

`git branch -a`: Show all existing branches. The branch you are currently writing to is marked with a *-character.

`git branch BRANCH_NAME`: Create a local branch with the name BRANCH_NAME.

`git checkout -b BRANCH_NAME`: Creates and switches to the branch BRANCH_NAME.

`git checkout BRANCH_NAME`: Switch to the branch BRANCH_NAME.

`git checkout FILENAME`: Undo changes of a file with name FILENAME.

`git stash`: Remove changes temporary (Changes are now in the stash).

`git stach pop`: Gets the changes back from the stash.

`git push --set-upstream origin BRANCH_NAME`: Only needed when you want to push a local branch the first time. Uploads the branch to the remote repository.

`git push origin BRANCH_NAME`: Push the branch BRANCH_NAME to the remote repository.

`git push`: Push the branch you are located at to the remote repository.

`git pull origin BRANCH_NAME`: Pull the branch BRANCH_NAME from the remote repository.

`git pull`: Pulls your current branch from the remote repository.

`git add FILENAME`: Adds the file with the name FILENAME to the branch, and prepares it for the next commit.

`git add --all`: Adds all changed files to the branch, and prepares them for the commit.

`git commit -m 'DESCRIPTION_OF_COMMIT'`: Saves all added files to your local repository.

`git tag TAG_NAME -m 'TAG_DESCRIPTION'`: Has to be executed after a commit. Tags the commit with the human readable name TAG_NAME.

`git push origin TAG_NAME`: Push the tag with name TAG_NAME to the remote repository.

`git diff FILENAME`: See the changes in the file FILENAME, compared to the last committed version, before you added it.

`git diff --cached FILENAME`: See the changes in the file FILENAME, compared to the last committed version, after you added it.

## Restore a previous state of your repository

Find the COMMIT_ID of the state you want to go back to. You can find the COMMIT_ID from the terminal using `git log` or from the browser version under `History` (Next to `Web IDE` and `Clone`).

`git checkout COMMIT_ID .`: Goes back to the COMMIT_ID on the current branch. Insted of the `.` you can type a BRANCH_NAME, which will bring you back to the commit on on the corresponding branch.

Now you are back to the previous state of COMMIT_ID on your local repository. To push the changes to the remote repository you have to execute `git add --all` and `git push origint BRANCH_NAME`, like for a any other commit.
After that your remote reposiyory is set back to the state of COMMIT_ID.
